## My Dockers

Getting started with node `http.createServer` see `server.js` and `Dockerfile`

## Cheatsheet

https://www.docker.com/sites/default/files/Docker_CheatSheet_08.09.2016_0.pdf

## Configure docker


https://askubuntu.com/questions/477551/how-can-i-use-docker-without-sudo

sudo setfacl -m user:$USER:rw /var/run/docker.sock

## Build and Run Docker Image

```sh
docker build -t tarnau-app:1.0 . 

docker images

docker container ls --all

docker run tarnau-app:1.0

docker run -p 7070:8080 tarnau-app:1.0

docker kill CONTAINER_ID
```
