FROM node:10-alpine
EXPOSE 8080
RUN mkdir /src
WORKDIR /src
COPY src/server.js .
CMD node server.js